# cert_reminder

Small script for querying cert-manager.com for soon-to-expire certificates.  Sends (configurable) email with this collected data in 4 sections:

- Certificates that will expire in the next N days, where N=
	- 2
	- 7
	- 30
- Certificates that *have* expired in the last 14 days

## Setup

To install:

  1. Clone this repository
  2. Copy `cert_reminder.cfg.example` to `cert_reminder.cfg`
  3. Fill in the `login` and `password` fields.
  4. Install 'jinja2' with `pip`: `pip install jinja1`.

Python >= 3.6 is required.

## Usage

Run `cert_reminder` like this:

> python3 cert_reminder.py --cfg /path/to/cert_reminder.cfg

More formally, `cert_reminder.py` takes one argument, `--cfg`, which specifies the location of the .cfg file required for execution.  This defaults to "./cert_reminder.cfg".

## Config

`cert_reminder` requires the presence of a file named `cert_reminder.cfg` containing the following sections and fields:

- api
  - `login` - API username
  - `password` - API password
  - `request_host` - API host
  - `request_endpoint` - API endpoint
- email
  - `recipient` - Email recipient address
  - `sender` - Email sending address
  - `subject` - Email subject line
  - `smtp_server` - SMTP server hostname
- headers
  - `this_month` - Header text for certs expiring this month
  - `this_week`- Header text for certs expiring this week
  - `tomorrow` - Header text for certs expiring tomorrow (or next business day)
  - `last_2_weeks` - Header text for certs that have expired in the last 2 weeks
- jinja
  - `template_dir` - Location of directory to search for jinja templates.

`cert_reminder.cfg.example` contains sensible defaults for most of these options.
