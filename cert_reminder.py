import argparse
import configparser
import datetime
import http.client
import json
import pathlib 
import smtplib
import jinja2
import sys
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import Dict, List

config = configparser.ConfigParser()


class CertificateStatus:
    """Contains named integers representing the Sectigo API's "CertificateStatus" attribute

    See: https://www.sectigo.com/uploads/audio/Certificate-Manager-20.1-Rest-API.html#reports-resource
    """

    ANY = 0
    REQUESTED = 1
    ISSUED = 2
    REVOKED = 3
    EXPIRED = 4
    ENROLLED_PENDING_DOWNLOAD = 5
    NOT_ENROLLED = 6
    AWAITING_APPROVAL = 7
    APPROVED = 8
    APPLIED = 9
    DOWNLOADED = 10


class DateAttribute:
    """Contains named integers representing the Sectigo API's "DateAttribute" attribute

    https://www.sectigo.com/uploads/audio/Certificate-Manager-20.1-Rest-API.html#reports-resource
    """

    ENROLLMENT = 0
    DOWNLOADING = 1
    REVOCATION = 2
    EXPIRATION = 3
    REQUEST = 4
    ISSUANCE = 5
    INVITATION = 6


def make_request(
    data: Dict[str, str], connection: http.client.HTTPSConnection
) -> http.client.HTTPResponse:
    """
    Sends a request with standard headers and given data using given connection object.
    """
    connection.request(
        "POST",
        config.get("api", "request_endpoint"),
        json.dumps(data),
        {
            "login": config.get("api", "login"),
            "password": config.get("api", "password"),
            "customerUri": "InCommon",
            "Content-Type": "application/json;charset=utf-8",
        },
    )
    response = connection.getresponse()
    return json.loads(response.read().decode("utf-8"))


def get_certificates_by_status(
    start_date: datetime.date,
    end_date: datetime.date,
    cert_status: int,
    date_attribute: int,
    connection: http.client.HTTPSConnection,
) -> Dict[str, str]:
    """Gets a list of certificates with a given status and given date attribute within given date range."""
    return make_request(
        {
            "from": start_date.isoformat(),
            "to": end_date.isoformat(),
            "certificateStatus": cert_status,
            "certificateDateAttribute": date_attribute,
        },
        connection,
    )


def newer_cert_exists(cert: Dict, connection: http.client.HTTPSConnection) -> bool:
    """Given a certificate dict, looks up matching CNs and returns a bool representing whether newer duplicates exist"""

    def timestamp_to_date(date_string: str) -> datetime.date:
        return datetime.datetime.strptime(date_string.split("T")[0], "%Y-%m-%d").date()

    common_name = cert["commonName"]
    expiration_date = timestamp_to_date(cert["expires"])
    certs_with_matching_cn = make_request(
        {"certificateStatus": CertificateStatus.ISSUED, "commonName": common_name},
        connection,
    )["reports"]

    for cert in certs_with_matching_cn:
        if expiration_date < timestamp_to_date(cert["expires"]):
            return True

def next_workday(start_date: datetime.date, min_jump: datetime.timedelta) -> datetime.date:
    """Jump forward `min_jump` days from today, then find the next weekday and return its date""" 
    todays_date = datetime.date.today()
    ret = todays_date + min_jump
    while ret.weekday() in [5, 6]:
        ret += datetime.timedelta(days=1)
    return ret


def last_two_weeks(end_date: datetime.date) -> datetime.date:
    """Returns a date 2 weeks prior to end_date"""
    return end_date - datetime.timedelta(weeks=2)


def send_mail(body: str):
    """
    Construct and send message
    """
    
    recipients = config.get("email", "recipient").split(',')

    msg = MIMEMultipart()
    msg["from"] = config.get("email", "sender")
    msg["to"] = config.get("email", "recipient")
    msg["subject"] = config.get("email", "subject")
    msg.attach(MIMEText(body, "html"))
    server = smtplib.SMTP()
    server.connect(config.get("email", "smtp_server"))
    server.sendmail(
        config.get("email", "sender"),
        recipients,    
        msg.as_string(),
    )
    server.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--cfg",
        required=False,
        type=pathlib.Path,
        default=pathlib.Path("cert_reminder.cfg"),
        help="Config file path",
    )
    parser.add_argument(
        "--print",
        action="store_true",
        default=False,
        help="Print results instead of emailing",
    )
    args = parser.parse_args(sys.argv[1:])
    config.read(args.cfg)
    today = datetime.date.today()

    jinja_environment = jinja2.Environment(
        loader=jinja2.FileSystemLoader(config.get("jinja", "template_dir")),
        autoescape=jinja2.select_autoescape()
    )
    message_template = jinja_environment.get_template("template.html.j2")

    reports = {
        "tomorrow": (today, next_workday(today, datetime.timedelta(days=0)), CertificateStatus.ISSUED),
        "this_week": (today, next_workday(today, datetime.timedelta(days=7)), CertificateStatus.ISSUED),
        "this_month": (today, next_workday(today, datetime.timedelta(days=30)), CertificateStatus.ISSUED),
        "last_2_weeks": (last_two_weeks(today), today, CertificateStatus.EXPIRED)
    }

    server = http.client.HTTPSConnection(config.get("api", "request_host"))
    responses: Dict[str, List[Dict[str, str]]] = {
        report_name: get_certificates_by_status(
            start_date, end_date, status, DateAttribute.EXPIRATION, server
        )["reports"]
        for report_name, (start_date, end_date, status) in reports.items()
    }
    server.close()

    # Sort sooner expiry first
    for name, cert_list in responses.items():
        cert_list.sort(key=lambda d: d["expires"])

    # Deduplicate
    for cert in responses["tomorrow"]:
        if cert in responses["this_week"]:
            responses["this_week"].remove(cert)
            responses["this_month"].remove(cert)
    for cert in responses["this_week"]:
        if cert in responses["this_month"]:
            responses["this_month"].remove(cert)

    # For remaining certs in each table, check if there is a newer, valid cert for the same CN
    # If yes, remove the redundant cert
    for cert_list in responses.values():
        for cert in cert_list:
            if newer_cert_exists(cert, server):
                cert_list.remove(cert)

    headings = {key: config.get("headers", key) for key in ("tomorrow", "this_week", "this_month", "last_2_weeks")}

    for key in list(responses.keys()):
        if responses[key] == list():
            del responses[key]

    # Render into our email template
    result = message_template.render(responses=responses, headings=headings)

    print(result) if args.print else send_mail(result)


if __name__ == "__main__":
    main()
